package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"text/template"
)

func main() {
	http.HandleFunc("/", handleRequest)

	fmt.Printf("Started HTTP server...\n")
	if err := http.ListenAndServe(":8000", nil); err != nil {
		log.Fatal(err)
	}
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "404 not found.", http.StatusNotFound)
		return
	}

	switch r.Method {
	case "GET":
		http.ServeFile(w, r, "index.html")
	case "POST":
		reqBody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Fprintf(w, convertTemplate(reqBody))
	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}
}

func convertTemplate(formData []byte) string {
	var receivedData Input
	if err := json.Unmarshal(formData, &receivedData); err != nil {
		panic(err)
	}

	var detectedTemplate string
	rex := regexp.MustCompile(`(?ms).*\[\[(.*)\]\].*\n.*palette\s*=.*"(#.*)?:(#.*):(#.*):(#.*):(#.*):(#.*):(#.*):(#.*):(#.*):(#.*):(#.*):(#.*):(#.*):(#.*):(#.*):(#.*)".*background_color\s*=\s*"(.*)".*cursor_color\s*=\s*"(#.*)".*foreground_color\s*=\s*"(#.*)".*\n.*`)
	if rex.MatchString(receivedData.InputData) {
		detectedTemplate = "Terminator"
	}
	m, _ := regexp.MatchString(`(?m)"terminal\.ansiBlack"`, string(receivedData.InputData))
	if m {
		detectedTemplate = "VSCode"
	}
	m, _ = regexp.MatchString(`(?m)"brightYellow"`, string(receivedData.InputData))
	if m {
		detectedTemplate = "WindowsTerminal"
	}

	var data WindowsTerminal
	var vsCodeData VSCode

	switch detectedTemplate {
	case "WindowsTerminal":
		err := json.Unmarshal([]byte(receivedData.InputData), &data)
		if err != nil {
			panic(err)
		}
	case "VSCode":
		err := json.Unmarshal([]byte(receivedData.InputData), &vsCodeData)
		if err != nil {
			panic(err)
		}
		data.Foreground = vsCodeData.WorkbenchColorCustomizations.TerminalForeground
		data.Background = vsCodeData.WorkbenchColorCustomizations.TerminalBackground
		data.Black = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiBlack
		data.BrightBlack = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiBrightBlack
		data.Blue = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiBlack
		data.BrightBlue = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiBrightBlue
		data.Red = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiRed
		data.BrightRed = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiBrightRed
		data.Cyan = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiCyan
		data.BrightCyan = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiBrightCyan
		data.Green = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiGreen
		data.BrightGreen = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiBrightGreen
		data.Purple = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiMagenta
		data.BrightPurple = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiBrightMagenta
		data.White = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiWhite
		data.BrightWhite = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiBrightWhite
		data.Yellow = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiYellow
		data.BrightYellow = vsCodeData.WorkbenchColorCustomizations.TerminalAnsiBrightYellow
	case "Terminator":
		match := rex.FindAllStringSubmatch(receivedData.InputData, -1)
		data.Name = match[0][1]
		data.Foreground = match[0][20]
		data.Background = match[0][18]
		data.Black = match[0][2]
		data.BrightBlack = match[0][10]
		data.Blue = match[0][6]
		data.BrightBlue = match[0][14]
		data.Red = match[0][3]
		data.BrightRed = match[0][11]
		data.Cyan = match[0][8]
		data.BrightCyan = match[0][16]
		data.Green = match[0][4]
		data.BrightGreen = match[0][12]
		data.Purple = match[0][7]
		data.BrightPurple = match[0][15]
		data.White = match[0][9]
		data.BrightWhite = match[0][17]
		data.Yellow = match[0][5]
		data.BrightYellow = match[0][13]
		data.CursorColor = match[0][19]
	}

	var returnedTemplate string
	switch receivedData.OutputFormat {
	case "Terminator":
		const terminatorTemplate = `[[{{.Name}}]]
    palette = "{{.Black}}:{{.Red}}:{{.Green}}:{{.Yellow}}:{{.Blue}}:{{.Purple}}:{{.Cyan}}:{{.White}}:{{.BrightBlack}}:{{.BrightRed}}:{{.BrightGreen}}:{{.BrightYellow}}:{{.BrightBlue}}:{{.BrightPurple}}:{{.BrightCyan}}:{{.BrightWhite}}"
    background_color = "{{.Background}}"
    cursor_color = "{{.CursorColor}}"
    foreground_color = "{{.Foreground}}"
    background_image = None`

		t, err := template.New("terminatorTemplate").Parse(terminatorTemplate)
		if err != nil {
			panic(err)
		}
		tpl := new(bytes.Buffer)
		err = t.Execute(tpl, data)
		if err != nil {
			panic(err)
		}
		returnedTemplate = tpl.String()
	case "VSCode":
		var code VSCode

		code.WorkbenchColorCustomizations.TerminalAnsiBlack = data.Black
		code.WorkbenchColorCustomizations.TerminalAnsiBrightBlack = data.BrightBlack
		code.WorkbenchColorCustomizations.TerminalAnsiBlue = data.Blue
		code.WorkbenchColorCustomizations.TerminalAnsiBrightBlue = data.BrightBlue
		code.WorkbenchColorCustomizations.TerminalAnsiCyan = data.Cyan
		code.WorkbenchColorCustomizations.TerminalAnsiBrightCyan = data.BrightCyan
		code.WorkbenchColorCustomizations.TerminalAnsiGreen = data.Green
		code.WorkbenchColorCustomizations.TerminalAnsiBrightGreen = data.BrightGreen
		code.WorkbenchColorCustomizations.TerminalAnsiMagenta = data.Purple
		code.WorkbenchColorCustomizations.TerminalAnsiBrightMagenta = data.BrightPurple
		code.WorkbenchColorCustomizations.TerminalAnsiRed = data.Red
		code.WorkbenchColorCustomizations.TerminalAnsiBrightRed = data.BrightRed
		code.WorkbenchColorCustomizations.TerminalAnsiYellow = data.Yellow
		code.WorkbenchColorCustomizations.TerminalAnsiBrightYellow = data.BrightYellow
		code.WorkbenchColorCustomizations.TerminalAnsiWhite = data.White
		code.WorkbenchColorCustomizations.TerminalAnsiBrightWhite = data.BrightWhite
		code.WorkbenchColorCustomizations.TerminalForeground = data.Foreground
		code.WorkbenchColorCustomizations.TerminalBackground = data.Background

		b, err := json.MarshalIndent(code, "", "  ")
		if err != nil {
			fmt.Printf("Error: %s", err)
		}
		returnedTemplate = (string(b))
	case "WindowsTerminal":
		b, err := json.MarshalIndent(data, "", "  ")
		if err != nil {
			fmt.Printf("Error: %s", err)
		}
		returnedTemplate = (string(b))

	}

	return returnedTemplate
}
