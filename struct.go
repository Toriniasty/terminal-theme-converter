package main

type Input struct {
	InputData    string `json:"inputData"`
	OutputFormat string `json:"outputFormat"`
}

type VSCode struct {
	WorkbenchColorCustomizations struct {
		TerminalBackground        string `json:"terminal.background"`
		TerminalForeground        string `json:"terminal.foreground"`
		TerminalCursorBackground  string `json:"terminalCursor.background"`
		TerminalCursorForeground  string `json:"terminalCursor.foreground"`
		TerminalAnsiBlack         string `json:"terminal.ansiBlack"`
		TerminalAnsiBlue          string `json:"terminal.ansiBlue"`
		TerminalAnsiBrightBlack   string `json:"terminal.ansiBrightBlack"`
		TerminalAnsiBrightBlue    string `json:"terminal.ansiBrightBlue"`
		TerminalAnsiBrightCyan    string `json:"terminal.ansiBrightCyan"`
		TerminalAnsiBrightGreen   string `json:"terminal.ansiBrightGreen"`
		TerminalAnsiBrightMagenta string `json:"terminal.ansiBrightMagenta"`
		TerminalAnsiBrightRed     string `json:"terminal.ansiBrightRed"`
		TerminalAnsiBrightWhite   string `json:"terminal.ansiBrightWhite"`
		TerminalAnsiBrightYellow  string `json:"terminal.ansiBrightYellow"`
		TerminalAnsiCyan          string `json:"terminal.ansiCyan"`
		TerminalAnsiGreen         string `json:"terminal.ansiGreen"`
		TerminalAnsiMagenta       string `json:"terminal.ansiMagenta"`
		TerminalAnsiRed           string `json:"terminal.ansiRed"`
		TerminalAnsiWhite         string `json:"terminal.ansiWhite"`
		TerminalAnsiYellow        string `json:"terminal.ansiYellow"`
	} `json:"workbench.colorCustomizations"`
}

type WindowsTerminal struct {
	Name                string `json:"name"`
	Black               string `json:"black"`
	Red                 string `json:"red"`
	Green               string `json:"green"`
	Yellow              string `json:"yellow"`
	Blue                string `json:"blue"`
	Purple              string `json:"purple"`
	Cyan                string `json:"cyan"`
	White               string `json:"white"`
	BrightBlack         string `json:"brightBlack"`
	BrightRed           string `json:"brightRed"`
	BrightGreen         string `json:"brightGreen"`
	BrightYellow        string `json:"brightYellow"`
	BrightBlue          string `json:"brightBlue"`
	BrightPurple        string `json:"brightPurple"`
	BrightCyan          string `json:"brightCyan"`
	BrightWhite         string `json:"brightWhite"`
	Background          string `json:"background"`
	Foreground          string `json:"foreground"`
	SelectionBackground string `json:"selectionBackground"`
	CursorColor         string `json:"cursorColor"`
}
